package app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PwaVaadinApplication {

	public static void main(String[] args) {
		SpringApplication.run(PwaVaadinApplication.class, args);
	}

}
